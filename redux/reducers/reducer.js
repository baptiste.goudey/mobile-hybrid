export default (state = {}, action) => {
    switch (action.type) {
      case 'LOGIN_SUCCESS':
        return ({ 
          email: action.value, });
      default:
        return state;
    }
  };