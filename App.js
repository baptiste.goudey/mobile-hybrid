import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { Asset, AppLoading } from 'expo';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import reducers from "./redux/reducers";
import AppRoot from './navigation/index';



const store = createStore(reducers, applyMiddleware(reduxThunk));


export default class App extends React.Component {
constructor(props) {
  super(props);
  this.state = {
    isLoadingComplete: false
  };
  /* if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  } */
}



loadResourcesAsync = async () => Promise.all([
  Expo.Font.loadAsync({
    'Roboto': require('native-base/Fonts/Roboto.ttf'),
    'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
  })
]);

handleLoadingError = (error) => {
  // In this case, you might want to report the error to your error
  // reporting service, for example Sentry
  console.warn(error);
};

handleFinishLoading = () => {
  this.setState({ isLoadingComplete: true });
};

  render() {
    if (!this.state.isLoadingComplete) {
      return (
        <AppLoading
          startAsync={this.loadResourcesAsync}
          onError={this.handleLoadingError}
          onFinish={this.handleFinishLoading}
        />
      );
    }
    return (
      <Provider store={store}>
        <AppRoot />
      </Provider>  
    );
  }
}
