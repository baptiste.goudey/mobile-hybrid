import * as firebase from 'firebase';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCu5RLrIzc1cbxWBcBK-nMI9fO_qYd-gE8",
    authDomain: "mobile-hybrid-9efa0.firebaseapp.com",
    databaseURL: "https://mobile-hybrid-9efa0.firebaseio.com",
    projectId: "mobile-hybrid-9efa0",
    storageBucket: "mobile-hybrid-9efa0.appspot.com",
    messagingSenderId: "1058616800117"
  };

firebase.initializeApp(firebaseConfig);

export const authRef = firebase.auth();
const firestore = firebase.firestore();
export const UsersRef = firestore.collection('users');
export const storage = firebase.storage();


