import React, { Component } from 'react';
import { WebView, View } from 'react-native';

export default class VideoWebScreen extends React.Component {

  render() {
    const { navigation } = this.props;
    const itemId = navigation.getParam('url', 'https://facebook.github.io/react-native/docs/navigation');
    return (
      <View style={{ flex: 1 }}>
        <WebView
          javaScriptEnabled={true}
          domStorageEnabled={true}
          source={{ html: "<html><body><iframe width='100%' src='https://www.youtube.com/embed/" + itemId + "' frameborder='0' allowfullscreen></iframe></body></html>" }}
          style={{ marginTop: 20 }}
        />
      </View>
    );
  }
}

