import React from 'react';
import { connect } from "react-redux";
import propTypes from 'prop-types';
import { authRef, UsersRef } from "../config/firebase";
import { setPersonData } from "../redux/actions/index";
import { View, StyleSheet, Dimensions, Image, ActivityIndicator } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';


export default class VideoTab extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      articles: null,
      refresh: false,
    };
  }


  getNews() {
    return fetch('https://www.googleapis.com/youtube/v3/search?key=AIzaSyA2HyShK47Y8soKeVR5pyVhDnOJ3n4Lqj0&channelId=UCSPEjw8F2nQDtmUKPFNF7_A&part=snippet,id&order=date&maxResults=30')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ articles: responseJson.items });
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
      });
  }

  goToWeb = (link) => {
    console.log(this.props);
    this.setState({ refresh: true });
    this.props.navigation.navigate('Web', { url: link });
    console.log(link);
    this.setState({ refresh: false });
  }

  componentDidMount() {
    this.getNews();
  }

  render() {
    const { articles } = this.state;
    if (articles == null) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }
    else {
      return (
        <Container>
          <Content>
            {articles.map(element =>
              <Card key={element.snippet.publishedAt} style={{ flex: 0 }}>
                <CardItem>
                  <Left>
                    <Body>
                      <Text>{element.snippet.title}</Text>
                      <Text note>{element.snippet.channelTitle}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem>
                  <Body>
                    {element.image != "" &&
                      <Image source={{ uri: element.snippet.thumbnails.default.url }} style={{ height: 200, width: 200, flex: 1, alignSelf: 'center' }} />}
                    <Text>
                      {element.snippet.description}
                    </Text>
                  </Body>
                </CardItem>
                <CardItem>
                  <Left>
                    <Button transparent onPress={() => this.props.navigation.navigate('WebVideo', { url: element.id.videoId })} textStyle={{ color: '#87838B' }}>
                      <Icon name="link" />
                      <Text>Watch it !</Text>
                    </Button>
                  </Left>
                </CardItem>
              </Card>
            )}
          </Content>
        </Container>
      );
    }
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
});

VideoTab.propTypes = {
  email: propTypes.string,
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired
  }).isRequired,
};