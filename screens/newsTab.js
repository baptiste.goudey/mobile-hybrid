import React from 'react';
import { connect } from "react-redux";
import propTypes from 'prop-types';
import { authRef, UsersRef } from "../config/firebase";
import { setPersonData } from "../redux/actions/index";
import { View, StyleSheet, Dimensions, Image, ActivityIndicator } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';


export default class NewsTab extends React.Component {
    static navigationOptions = {
        header: null,
      };

    constructor(props) {
      super(props);
      this.state = {
        articles : undefined,
        refresh : false,
      };
    }
    
    
    getNews() {
      return fetch('https://gapi.xyz/api/v2/?q=' + this.props.news + '&token=defa6e6d146a2d2596193f82f3163c9e')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({articles : responseJson.articles});
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
      });
    }

    goToWeb = (link) => {
      console.log(this.props);
      this.setState({refresh : true});
      this.props.navigation.navigate('Web', {url : link } );
      this.setState({refresh : false});
    }
    
    componentDidMount() {
      this.getNews();
    }

    render() {
        const {articles} = this.state;
        if(articles == undefined){
            return (
                <View style={styles.container}>
                  <ActivityIndicator size="large" color="#0000ff" />
                </View>
              );
        }
        return(
            <Container>
            <Content>
            {articles.map(element => 
                <Card key={element.date} style={{flex: 0}}>
                <CardItem>
                  <Left>
                    <Body>
                      <Text>{element.title}</Text>
                      <Text note>{element.source}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem>
                  <Body>
                    {element.image != "" &&  
                    <Image source={{uri: element.image}} style={{height: 200, width: 200, flex: 1, alignSelf: 'center'}}/>}
                    <Text style={{marginBottom : -20}}>
                      {element.desc}
                    </Text>
                  </Body>
                </CardItem>
                <CardItem >
                  <Left>
                    <Button transparent onPress={() => this.props.navigation.navigate('WebText', {url : element.link })} textStyle={{color: '#87838B'}}>
                      <Icon name="link" />
                      <Text>Read it !</Text>
                    </Button>
                  </Left>
                </CardItem>
              </Card>
              )}
            </Content>
          </Container>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center'
    },
  });

  NewsTab.propTypes = {
    email: propTypes.string,
    navigation: propTypes.shape({
      navigate: propTypes.func.isRequired}).isRequired,
  };