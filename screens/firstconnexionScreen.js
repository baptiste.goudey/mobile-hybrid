import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Container, Text, Header, Content, Form, Item, Input, Label, Button, Icon } from 'native-base';
import { connect } from "react-redux";
import { authRef, UsersRef, storage } from "../config/firebase";
import propTypes from 'prop-types';
import { setPersonData } from "../redux/actions/index";
import CustomCamera from '../components/camera';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';



class FirstConnexionScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle:
        <View style={{ flex: 1 }}>
          <Text >Profil</Text></View>,
      headerLeft: <Button transparent onPress={() => navigation.navigate('Home')}>
        <Icon name='arrow-back' />
      </Button>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      description: '',
      display: false,
      photo: 'https://facebook.github.io/react-native/docs/assets/favicon.png',
    };
  }

  updateProfile = () => {
    /* const userref = db.collection('users').where(this.state.email).get().then((querySnapshot) => {
      console.log('start');
        querySnapshot.forEach(doc => {
          console.log('aaaaa');
            console.log(doc.data());
            // Build doc ref from doc.id
            //db.collection("users").doc(doc.id).update({description: this.state.description, username : this.state.username});
        });
     console.log('end');   
}).catch((error) => {
console.log(error);
}) */
    UsersRef.doc(this.props.email).update({ description: this.state.description, username: this.state.username, photo: this.state.photo });
    this.props.navigation.navigate('Home');
  }

  getProfile = () => {
    UsersRef.doc(this.props.email).get()
      .then(doc => {
        this.setState({ description: doc.data()['description'], username: doc.data()['username'] });
        if (doc.data()['photo'] !== undefined)
          this.setState({ photo: doc.data()['photo'] });
      });
  }

  handlePicture = async (photo) => {
    this.setState({ display: false });
    this.uploadImage(photo, authRef.currentUser.uid);
  }

  pickImage = async () => {
    const photo = await ImagePicker.launchImageLibraryAsync(ImagePicker.MediaTypeOptions.Image);
    if (!photo.cancelled)
      this.uploadImage(photo, authRef.currentUser.uid);

  }

  async uploadImage(picture, uid) {
    const storageRef = storage.ref(uid);
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function (e) {
        reject(new TypeError('Network request failed'));
      };
      xhr.responseType = 'blob';
      xhr.open('GET', picture.uri, true);
      xhr.send(null);
    });

    const metadata = {
      contentType: 'image/jpeg',
    };

    try {
      storageRef.put(blob, metadata).then(snapshot => {
        snapshot.ref.getDownloadURL().then(downloadURL => {
          this.setState({ photo: downloadURL });
        });
      });
    } catch (err) {
      reject(err);
    }
  }




  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    //const {permission} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({ hasCameraPermission: status === 'granted' });
    this.getProfile();
  }

  render() {
    const { display, photo } = this.state;
    if (display === true) {
      return (<CustomCamera display={display} pictureHandler={this.handlePicture} />);
    }
    else {
      return (
        <Container>
          <Content style={styles.container}>
            <Image source={{ uri: photo }} style={{ width: 150, height: 150, alignSelf: 'center' }} />
            <Form style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }} >
              <Icon type="FontAwesome" name="camera" style={{ fontSize: 20 }} onPress={() => { this.setState({ display: true }); }} />
              <Icon type="FontAwesome" name="upload" style={{ fontSize: 20 }} onPress={this.pickImage} />
            </Form>
            <Form>
              <Item floatingLabel style={{ marginBottom: 20, marginTop: 50 }}>
                <Label>Username</Label>
                <Input onChangeText={(username) => this.setState({ username })} value={this.state.username} />
              </Item>
              <Item floatingLabel style={{ marginBottom: 20 }}>
                <Label>Description</Label>
                <Input onChangeText={(description) => this.setState({ description })} value={this.state.description} />
              </Item>
              <Button block light onPress={this.updateProfile} style={{ width: 400, alignSelf: 'center' }} >
                <Text>Completer mon profil</Text>
              </Button>
            </Form>
          </Content>
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});


const mapStateToProps = (state) => {
  return {
    email: state.data.email
  };
}

const mapDispatchToProps = (dispatch) => ({
  loginRequest: data => dispatch(setPersonData(data)),
});

FirstConnexionScreen.propTypes = {
  loginRequest: propTypes.func.isRequired,
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired
  }).isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(FirstConnexionScreen);