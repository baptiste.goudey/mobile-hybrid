import React from 'react';
import { connect } from "react-redux";
import propTypes from 'prop-types';
import { authRef, UsersRef } from "../config/firebase";
import { setPersonData } from "../redux/actions/index";
import { View, StyleSheet, Dimensions, Image } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, Title } from 'native-base';
import NewsTab from "./newsTab";
import VideoTab from "./videoTab";

class HomeScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle:
        <View style={{ flex: 1 }}>
          <Text style={{alignSelf: 'center', marginLeft: -20}} >AsiaMedia</Text></View>,
      headerLeft: <Button transparent onPress={() => navigation.navigate('FirstConnexion')}>
        <Icon type='MaterialCommunityIcons' name='account' />
      </Button>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      description: '',
      index: 0,
      routes: [
        { key: 'first', title: 'Japan' },
        { key: 'second', title: 'Korea' },
        { key: 'third', title: 'Video' },
      ],
      articles: {}
    };
  }

  componentDidMount() {
  }

  _renderScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return <NewsTab {...this.props} news={"japan"} />;
      case 'second':
        return <NewsTab {...this.props} news={"korea"} />;
      case 'third':
        return <VideoTab {...this.props} />;
      default:
        return null;
    }
  }



  render() {
    return (

      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height - 100 }}
      />


    );
  }
}


const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});

HomeScreen.propTypes = {
  email: propTypes.string,
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired
  }).isRequired,
};

HomeScreen.defaultProps = {
  email: '',
};


const mapStateToProps = (state) => {
  return ({
    email: state.data.email
  });
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginRequest: data => dispatch(setPersonData(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);