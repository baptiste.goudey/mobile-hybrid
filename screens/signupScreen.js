import React from 'react';
import { StyleSheet, View, TextInput, Alert, TouchableOpacity, Image } from 'react-native';
import { Container, Text, Header, Content, Form, Item, Input, Label, Button, Icon } from 'native-base';
import { connect } from "react-redux";
import { authRef, UsersRef } from "../config/firebase";
import propTypes from 'prop-types';
import { setPersonData } from "../redux/actions/index";


class SignUpScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  authFire = () => {
    const { loginRequest } = this.props;
    authRef
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(
        () => {
          UsersRef.doc(this.state.email).set({
            email: this.state.email
          })
          loginRequest(this.state.email);
          this.props.navigation.navigate('FirstConnexion')
        }
      )
      .catch((error) => {
        Alert.alert(
          'Sign Up Error',
          error.message,
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false }
        )
      }
      )
  }


  render() {
    return (
      <Container>
        <Content style={styles.container}>
          <Form  >
            <Image
              style={{ flex: 1, height: 200, width: undefined, marginRight: 10, marginLeft: 10, marginTop: 150 }}
              source={require('../assets/images/asiamedia.png')}
              resizeMode="contain"
            />
            <Item floatingLabel style={{ marginBottom: 20, marginTop: 50 }}>
              <Label>Email</Label>
              <Input onChangeText={(email) => this.setState({ email })} value={this.state.email} />
            </Item>
            <Item floatingLabel style={{ marginBottom: 20 }}>
              <Label>Password</Label>
              <Input onChangeText={(password) => this.setState({ password })} secureTextEntry value={this.state.password} />
            </Item>
            <Button block light onPress={this.authFire} style={{ width: 400, marginBottom: 20, alignSelf: 'center' }} >
              <Text>Sign Up</Text>
            </Button>
            <Button block light onPress={() => this.props.navigation.navigate('Login')} style={{ width: 400, alignSelf: 'center' }} >
              <Text>Already a user ?</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});

const mapStateToProps = (state) => {
  return {
    email: state.data.email
  };
}

const mapDispatchToProps = (dispatch) => ({
  loginRequest: data => dispatch(setPersonData(data)),
});

SignUpScreen.propTypes = {
  loginRequest: propTypes.func.isRequired,
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired
  }).isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);