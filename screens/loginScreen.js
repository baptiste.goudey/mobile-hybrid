import React from 'react';
import { StyleSheet, View, TextInput, Alert, TouchableOpacity, Image } from 'react-native';
import { Container, Text, Header, Content, Form, Item, Input, Label, Button, Icon } from 'native-base';
import { connect } from "react-redux";
import { authRef } from "../config/firebase";
import propTypes from 'prop-types';
import { setPersonData } from "../redux/actions/index";



class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }



  authFire = () => {
    const { loginRequest } = this.props;
    authRef.signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        loginRequest(this.state.email);
        this.props.navigation.navigate('Home')
      }
      )
      .catch((error) => {
        Alert.alert(
          'Sign In Error',
          error.message,
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false }
        )
      }
      )
  }


  componentDidMount() {

  }



  render() {
    return (
      <Container>
        <Content style={styles.container}>
          <Form  >
            <Image
              style={{ flex: 1, height: 200, width: undefined, marginRight: 10, marginLeft: 10, marginTop: 150 }}
              source={require('../assets/images/asiamedia.png')}
              resizeMode="contain"
            />
            <Item floatingLabel style={{ marginBottom: 20, marginTop: 50 }}>
              <Label>Email</Label>
              <Input onChangeText={(email) => this.setState({ email })} value={this.state.email} />
            </Item>
            <Item floatingLabel style={{ marginBottom: 20 }}>
              <Label>Password</Label>
              <Input onChangeText={(password) => this.setState({ password })} secureTextEntry value={this.state.password} />
            </Item>
            <Button block light onPress={this.authFire} style={{ width: 400, alignSelf: 'center', marginBottom: 20 }} >
              <Text>Sign In</Text>
            </Button>
            <Button block light onPress={() => this.props.navigation.navigate('SignUp')} style={{ width: 400, alignSelf: 'center' }} >
              <Text>Create an Account</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});

const mapStateToProps = (state) => {
  return {
    email: state.data.email
  };
}

const mapDispatchToProps = (dispatch) => ({
  loginRequest: data => dispatch(setPersonData(data)),
});

LoginScreen.propTypes = {
  loginRequest: propTypes.func.isRequired,
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired
  }).isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
