import React, { Component } from 'react';
import { WebView, View } from 'react-native';

export default class TextWebScreen extends React.Component {

  render() {
    const { navigation } = this.props;
    const itemId = navigation.getParam('url', 'https://facebook.github.io/react-native/docs/navigation');
    return (
      <View style={{ flex: 1 }}>
        <WebView
          javaScriptEnabled={true}
          domStorageEnabled={true}
          source={{uri : itemId}}
          style={{ marginTop: 20 }}
        />
      </View>
    );
  }
}

