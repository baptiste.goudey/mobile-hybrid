import { createStackNavigator, createAppContainer } from 'react-navigation';
import SignUpScreen from '../screens/signupScreen';
import HomeScreen from '../screens/homeScreen';
import LoginScreen from '../screens/loginScreen';
import FirstConnexionScreen from '../screens/firstconnexionScreen';
import NewsTab from '../screens/newsTab';
import VideoWebScreen from '../screens/videoWebScreen';
import TextWebScreen from '../screens/textWebScreen';

const nav = createStackNavigator(
{
  SignUp: SignUpScreen,
  Home : HomeScreen,
  Login : LoginScreen,
  FirstConnexion : FirstConnexionScreen,
  News : NewsTab,
  WebVideo : VideoWebScreen,
  WebText : TextWebScreen,
},
{
    initialRouteName: "Login"
},
{
  headerMode: 'none',
});

const App = createAppContainer(nav);

export default App;