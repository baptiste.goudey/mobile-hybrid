import React from 'react';
import * as Camera from 'expo-camera';
import propTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import { View, TouchableOpacity } from 'react-native';

export default class CustomCamera extends React.PureComponent {
  state = {
    type: Camera.Constants.Type.back,
  };

  snap = async () => {
    const { pictureHandler } = this.props;
    if (this.camera) {
      console.log('Taking picture');
      const photo = await this.camera.takePictureAsync();
      pictureHandler(photo);
    }
  }

  goBack = () => {
    const { pictureHandler } = this.props;
    pictureHandler();
  }

  render() {
    const { display } = this.props;
    const { type } = this.state;
    if (!display) {
      return <View />;
    }
    return (
      <View style={{ flex: 1 }}>
        <Camera ref={(ref) => { this.camera = ref; }} style={{ flex: 1 }} type={type} ratio="16:9" flashMode="auto">
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}
          >
            <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'flex-end' }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  alignItems: 'flex-start',
                  marginLeft: 20,
                }}
                onPress={this.goBack}
              >
                <Icon
                  name="ios-arrow-back"
                  type="ionicon"
                  size={52}
                  color="#ffffff"
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                  alignItems: 'center',
                }}
                onPress={this.snap}
              >
                <Icon
                  name="ios-camera"
                  type="ionicon"
                  size={52}
                  color="#ffffff"
                  style={{ flex: 1, alignSelf: 'flex-start', marginLeft: 25 }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 0.7,
                  marginRight: 20,
                  alignItems: 'flex-end',
                }}
                onPress={() => {
                  this.setState({
                    type: type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back,
                  });
                }}
              >
                <Icon
                  name="md-reverse-camera"
                  type="ionicon"
                  size={52}
                  color="#ffffff"
                />
              </TouchableOpacity>
            </View>
          </View>
        </Camera>
      </View>
    );
  }
}

CustomCamera.propTypes = {
  display: propTypes.bool,
  pictureHandler: propTypes.func.isRequired,
};

CustomCamera.defaultProps = {
  display: false,
};
